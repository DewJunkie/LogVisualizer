The goal of this project is to create a log viewer that is feature complete with Log2Console.  
I've been using Log2Console for a while, and for the most part it works really well.  There are times when it doesn't however.  One instance is when too many log events are sent in a short period of time, it seems to hang.  

So far this project supports Serilog Network UDP sources, and Serilog MongoDb source.  I've tried to set it up so that adding other sources should be reasonably trivial.

This project is developed using:  
-WPF  
-[Syncfusion](https://www.syncfusion.com/products/communitylicense) controls.  
-ReactiveUI




Building:  
- When debugging, UI Debugging for XAML causes an exception sometimes when changing the docking layout.  Either disable it or make any layout changes when not debugging.  
````
    Tools -> Options -> Debugging -> General ->  
    Uncheck: Enable UI Debugging Tools for XAML  
````
- Add the syncfusion wpf nuget source to your list of source.  
    http://nuget.syncfusion.com/wpf

