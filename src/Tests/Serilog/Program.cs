﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using GenFu;
using Serilog.Data;
using Serilog.Events;
using Serilog.Formatting.Compact;
using Serilog.Formatting.Json;
using Serilog.Formatting.Raw;
using Serilog.Sinks.Network;

namespace Serilog
{
    class Program
    {
        private static ILogger Logger;
        static void Main(string[] args)
        {
            Serilog.Debugging.SelfLog.Enable(msg => Console.WriteLine(msg));

            Serilog.Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.UDPSink(IPAddress.Loopback, 1337,textFormatter: new CompactJsonFormatter())
                .WriteTo.File("SerilogTest.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 3, outputTemplate: "{Timestamp},{Level},{Message},{Properties}{NewLine}")
                .WriteTo.File(new JsonFormatter(), "jsonLog.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 3)
                .WriteTo.File(new RenderedCompactJsonFormatter(), "compact.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 3)
                .WriteTo.File(new CompactJsonFormatter(), "compact2-.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 3)
                // Tracer current implementation uses $return $exception, mongo doesn't like that.  
                // So batchPostingLimit: 1 will capture the others, because the whole batch will fail.
                .WriteTo.MongoDBCapped("mongodb://10.30.0.61/logs", cappedMaxSizeMb:200L, batchPostingLimit:1) 
                //.WriteTo.Console()
                .CreateLogger();

            Logger = Log.Logger.ForContext<Program>();

            A.Configure<Person>()
                .Fill(p => p.Biography)
                .AsLoremIpsumSentences(3);

            var person = A.New<Person>();
            var people = A.ListOf<Person>();

            Logger.Information("{Name} is not safe {$Person} to deserialize", person.FirstName, person);
            Logger.Verbose("Hello World!");
            Logger.Information("Look a {Person}", person, person);
            Logger.Information("They are surrounded by {People}", people);
            Logger.Information("Look a {@Person}",                 person);
            Logger.Information("They are surrounded by {@People}", people);
            Logger.Verbose("Verbose");
            Logger.Information("Information");
            Logger.Warning("Warming");
            Logger.Error("Error");
            Logger.Fatal("Fatal");
            Logger.Error(new Exception("Exceptional!!"), "An exceptional exception.");

            person.Speak("Hey look I can talk");
            person.Divide(5, 10);
            try
            {
                person.Divide(99, 0);
            }
            catch (Exception e)
            {
                Logger.Information(e, "Can't do that");
            }

            
            Serilog.Log.CloseAndFlush();
            Console.ReadKey();
        }
    }
}
