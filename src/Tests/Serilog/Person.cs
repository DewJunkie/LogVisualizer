﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Serilog
{
    class Person
    {
        public string   UserName     {get;  set;}
        public string   FirstName    { get; set; }
        public string   MiddleName   {get;  set;}
        public string   LastName     { get; set; }
        public string   Title        { get; set; }
        public string   Gender       {get;  set;}
        public int      Age          { get; set; }
        public int      NumberOfKids { get; set; }
        public string   Address      {get;  set;}
        public string   City         {get;  set;}
        public string   State        {get;  set;}
        public string   Zip          {get;  set;}
        public string   Phone        {get;  set;}
        public DateTime DOB          {get;  set;}
        public string   Biography    {get;  set;}

        public int Divide(int x, int y)
        {
            return x / y;
        }

        public void Speak(string message)
        {
            OpenMouth();
            Tracer.Serilog.Log.Information("Ready to speak.");
            SaySomething(message);
            CloseMouth();
        }

        private void OpenMouth()
        {
            Thread.Sleep(150);
        }

        private void SaySomething(string message)
        {
            Console.WriteLine($"{FirstName} says {message}.");
        }

        private void CloseMouth()
        {

        }
    }
}
