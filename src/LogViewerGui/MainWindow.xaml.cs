﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LogViewerGui.MessageSources;
using LogViewerGui.Model;
using LogViewerGui.ViewModel;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using ReactiveUI;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.Windows.Shared;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace LogViewerGui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IViewFor<MainWindowViewModel>
    {
        private readonly string settingsPath;

        public MainWindow()
        {
            settingsPath = System.IO.Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                nameof(LogViewerGui),
                nameof(LogViewerGui) + ".json");
            InitializeComponent();

            this.Title = $"Log Viewer {Assembly.GetEntryAssembly().GetName().Version}";

            this.WhenAnyValue(v => v.ViewModel.Messages)
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(m =>
                {
                    DataGridMessages.ItemsSource = m;
                    try
                    {
                        DataGridMessages.Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(ViewModel.DataGridState)));
                    }
                    catch (Exception e)
                    {
                        Tracer.Serilog.Log.Information(e, "Error loading Datagrid");
                    }
                    DataGridMessages.AllowResizingHiddenColumns = true;
                    m.Changed.Select(_ => Unit.Default)
                        .Throttle(TimeSpan.FromMilliseconds(100))
                        .ObserveOn(RxApp.MainThreadScheduler)
                        .Subscribe(_ =>
                        {
                            TextBlockCount.Text                     = ViewModel.Messages.Count.ToString();
                            TextBoxDisplayed.Text = DataGridMessages.View.Records.Count.ToString();
                            var temp                                = DataGridMessages.SortColumnDescriptions;
                            DataGridMessages.SortColumnDescriptions = null;
                            DataGridMessages.SortColumnDescriptions = temp;
                        });

                });

            try
            {
                this.ViewModel = JsonConvert.DeserializeObject<MainWindowViewModel>(File.ReadAllText(settingsPath), new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
                DockingManager.LoadDockState(new StringReader(ViewModel.DockingState));
                //DataGridMessages.Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(ViewModel.DataGridState)));
            }
            catch(Exception ex)
            {
                Tracer.Serilog.Log.Information(ex, "Unable to load ViewModel");
                if (ViewModel == null)
                {
                    this.ViewModel      = new MainWindowViewModel();
                    var mongoDbSource   = new MongoDbSource();
                    mongoDbSource.DbUrl = "mongodb://log:log@127.0.0.1/logs";
                    ViewModel.LogSources.Add(mongoDbSource);
                }
            }

            this.WhenAnyValue(v => v.ViewModel)
                .Subscribe(vm =>
                {
                    vm.SaveCommand.Subscribe(_ => { _Save(); });
                });

            //var udpSource = new UdpMessageSource(1337);
            //MessageSources.Add(udpSource);
            //MessageSources.Add(mongoDbSource);

            this.Bind(ViewModel, vm => vm.SelectedMessage, v => v.DataGridMessages.SelectedItem);
            this.Bind(ViewModel, vm => vm.SelectedMessage.RawMessage, v => v.TextBoxRaw.Text);
            this.Bind(ViewModel, vm => vm.SearchText, v => v.TextBoxSearch.Text);
            this.Bind(ViewModel, vm => vm.WindowState.Height, v => v.Height);
            this.Bind(ViewModel, vm => vm.WindowState.Width, v => v.Width);
            this.Bind(ViewModel, vm => vm.WindowState.Left, v => v.Left);
            this.Bind(ViewModel, vm => vm.WindowState.Top, v => v.Top);
            this.Bind(ViewModel, vm => vm.SelectedLogSourceType, v => v.ComboBoxLoggerTypes.SelectedItem);
            this.Bind(ViewModel, vm => vm.SelectedLogSource, v => v.ListBoxSources.SelectedItem);
            this.Bind(ViewModel, vm => vm.KeepMessageCount, v => v.ComboBoxKeepMessageCount.Text);
            //this.OneWayBind(ViewModel, vm => vm.Contexts, v => v.ListBoxContexts.ItemsSource);
            this.WhenAnyValue(v => v.ViewModel.Contexts)
                .Subscribe(c => ListBoxContexts.ItemsSource = c);
            this.WhenAnyValue(v => v.ViewModel.LogSources)
                .Subscribe(s => ListBoxSources.ItemsSource = s);
            this.WhenAnyValue(v => v.ViewModel.LogSourceTypes)
                .Subscribe(lst => ComboBoxLoggerTypes.ItemsSource = lst);

            this.WhenAnyValue(v => v.ViewModel.SearchText)
                .Throttle(TimeSpan.FromMilliseconds(250))
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(s => DataGridMessages.SearchHelper.Search(s));

            this.BindCommand(ViewModel, vm => vm.ClearCommand, v => v.ButtonClear);
            this.BindCommand(ViewModel, vm => vm.ResizeColumnsCommand, v => v.ButtonAutoSize);
            this.WhenAnyValue(v => v.ViewModel.ResizeColumnsCommand)
                .Subscribe(c => c.Subscribe(_ =>
                {
                    DataGridMessages.GridColumnSizer.Refresh();
                    DataGridMessages.GridColumnSizer.ResetAutoCalculationforAllColumns();
                }));
            this.BindCommand(ViewModel, vm => vm.SaveCommand, v => v.ButtonSave);
            this.BindCommand(ViewModel, vm => vm.AddLogSourceComand, v => v.ButtonAddLogger);
            this.BindCommand(ViewModel, vm => vm.RemoveLogSourceCommand, v => v.ButtonRemoveLogger);
        }

        private void _Save()
        {
            var ss = new StringWriter();
            DockingManager.SaveDockState(ss);
            Tracer.Serilog.Log.Verbose(ss.ToString());
            ViewModel.DockingState = ss.ToString();
            var ms = new MemoryStream();
            DataGridMessages.Serialize(ms);
            ViewModel.DataGridState = Encoding.UTF8.GetString(ms.ToArray());
            FileInfo settingsFile = new FileInfo(settingsPath);
            if (!settingsFile.Directory.Exists)
            {
                settingsFile.Directory.Create();
            }
            File.WriteAllText(settingsPath, JsonConvert.SerializeObject(ViewModel, Formatting.Indented,
                new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Auto
                }));
            //Debug.WriteLine(json);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = value as MainWindowViewModel;
        }

        public MainWindowViewModel ViewModel { get; set; }

        private void DataGridMessages_OnAutoGeneratingColumn(object sender, AutoGeneratingColumnArgs e)
        {
            e.Column.MaximumWidth = System.Windows.SystemParameters.PrimaryScreenWidth * 2 / 3;
            switch (e.Column.MappingName)
            {
                case nameof(LogMessage.RawMessage):
                    e.Cancel = true;
                    break;
                case "Timestamp" when e.Column is GridDateTimeColumn dtCol: 
                    dtCol.Pattern       = DateTimePattern.CustomPattern;
                    dtCol.CustomPattern = "ddd HH:mm:ss.ffff";
                    break;
            }
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            ViewModel.SaveCommand.Execute().Subscribe();
            _Save();
            bool isExecuting = true;
            ViewModel.SaveCommand.IsExecuting
                .Where(executing => !executing)
                .Subscribe(executing => isExecuting = executing);
            while (isExecuting)
            {
                Thread.Sleep(100);
            }
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.WindowState = ViewModel.WindowState.State;
            this.Bind(ViewModel, vm => vm.WindowState.State, v => v.WindowState);
        }
    }
}
