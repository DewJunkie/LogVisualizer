﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace LogViewerGui.Util
{
    class DicitionaryContractResolver:DefaultContractResolver
    {
        readonly Dictionary<string,string> _propertyMap;
        public DicitionaryContractResolver(Dictionary<string,string> propertyMap)
        {
            _propertyMap = propertyMap;
        }
        protected override string ResolvePropertyName(string propertyName)
        {
            return _propertyMap.TryGetValue(propertyName, out var resolvedName) ? resolvedName : base.ResolvePropertyName(propertyName);
        }
    }
}
