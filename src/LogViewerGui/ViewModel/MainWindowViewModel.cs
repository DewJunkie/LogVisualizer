﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using LogViewerGui.LogSources;
using LogViewerGui.Model;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json.Linq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace LogViewerGui.ViewModel
{
    public class WindowState
    {
        public double Left { get; set; }
        public double Top { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public System.Windows.WindowState State { get; set; }
    }
    public class MainWindowViewModel : ReactiveObject
    {
        public MainWindowViewModel()
        {
            Contexts   = new ReactiveList<string>();
            LogSources = new ReactiveList<ILogSource>();
            Messages   = new ReactiveList<LogMessage>();
            LogSourceTypes = Assembly.GetAssembly(typeof(ILogSource)).GetTypes()
                .Where(t => typeof(ILogSource).IsAssignableFrom(t))
                .Where(t => t != typeof(ILogSource));

            this.WindowState = new WindowState();

            Messages.Changed
                .Buffer(TimeSpan.FromMilliseconds(250))
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(_ =>
                {
                    Contexts = new ReactiveList<string>(
                        Messages.Select(m => m.Context)
                            .Concat(Contexts)
                            .OrderBy(c => c)
                            .Distinct()
                    );
                });
            LogSources.ItemsAdded
                .Subscribe
                (ls =>
                {
                    ls.Messages
                        .Buffer(TimeSpan.FromMilliseconds(200))
                        .ObserveOn(RxApp.MainThreadScheduler)
                        .Subscribe(m =>
                        {
                            var supress = this.SuppressChangeNotifications();
                            Messages.AddRange(m);
                            while (Messages.Count > 500)
                            {
                                Messages.RemoveAt(0);
                            }

                            supress.Dispose();
                        });
                });
            ClearCommand         = ReactiveCommand.Create(() => Messages.Clear());
            ResizeColumnsCommand = ReactiveCommand.Create(() => Unit.Default);
            AddLogSourceComand = ReactiveCommand.Create(() =>
            {
                LogSources.Add((ILogSource)Activator.CreateInstance(SelectedLogSourceType));
            }, this.WhenAny(vm => vm.SelectedLogSourceType, s => s.Value != null));
            RemoveLogSourceCommand = ReactiveCommand.Create(() =>
            {
                LogSources.Remove(SelectedLogSource);
                SelectedLogSource.Dispose();
            }, this.WhenAny(vm => vm.SelectedLogSource, sls => sls.Value != null));
            SaveCommand          = ReactiveCommand.Create(() =>
            {
                Tracer.Serilog.Log.Debug("SaveCommand");
            });
        }

        [Reactive]
        public ReactiveList<LogMessage> Messages { get; set; }

        [Reactive]
        public int  KeepMessageCount { get;set; } 

        //[Reactive]public IReactiveDerivedList<LogMessage> MessagesDisplay { get; set; }
        [Reactive]
        public LogMessage SelectedMessage { get; set; }

        [Reactive]
        [DataMember]
        public ReactiveList<string> Contexts { get; set; }

        [Reactive]
        public string SearchText { get; set; }

        [Reactive]public IEnumerable<Type>   LogSourceTypes { get;set; }
        [Reactive]public Type   SelectedLogSourceType { get; set; }

        [DataMember]
        public WindowState WindowState { get; set; }
        [DataMember]
        public string DockingState { get; set; }
        [DataMember]
        public string DataGridState { get; set; }

        [Reactive]
        [DataMember]
        public ReactiveList<ILogSource> LogSources { get; set; }

        [Reactive]
        public ILogSource SelectedLogSource { get; set; }

        public ReactiveCommand<Unit, Unit> ClearCommand { get; private set; }

        public ReactiveCommand<Unit, Unit> ResizeColumnsCommand { get; private set; }

        public ReactiveCommand<Unit, Unit> SaveCommand { get; private set; }
        public ReactiveCommand<Unit, Unit> AddLogSourceComand { get; private set; }
        public ReactiveCommand<Unit,Unit> RemoveLogSourceCommand { get; private set; }
    }
}
