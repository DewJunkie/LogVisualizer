﻿using System;
using System.Reactive.Subjects;
using LogViewerGui.Model;

namespace LogViewerGui.LogSources
{
    public interface ILogSource:IDisposable
    {
        bool IsEnabled { get; set; }
        string  Name { get; set; }
        Subject<LogMessage> Messages { get; }
    }
}
