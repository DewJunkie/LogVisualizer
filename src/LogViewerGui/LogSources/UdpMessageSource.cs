﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using LogViewerGui.LogSources;
using LogViewerGui.Model;
using LogViewerGui.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Serilog.Events;
using Serilog.Formatting.Compact.Reader;

namespace LogViewerGui.MessageSources
{
    public class UdpMessageSource : ReactiveObject, ILogSource
    {
        private UdpClient  _udpClient;
        [Reactive][DataMember] public string Name { get;set; } = nameof(UdpMessageSource);
        [Reactive][DataMember]public bool IsEnabled { get; set; }
        private UInt16 id = 0;
        [Reactive][DataMember] public String ListenAddress { get; set; } = IPAddress.Any.ToString();
        [Reactive][DataMember] public ushort ListenPort { get; set; } = 1337;
        

        public Subject<LogMessage> Messages { get; private set; } = new Subject<LogMessage>();

        public UdpMessageSource()
        {
            this.WhenAnyValue(vm => vm.ListenAddress, vm => vm.ListenPort, vm => vm.IsEnabled,
                    (listenAddress, listenPort, isEnabled) => (listenAddress, listenPort, isEnabled))
                .Throttle(TimeSpan.FromMilliseconds(250))
                .Subscribe(p =>
                {
                    IPAddress.TryParse(ListenAddress, out var ipAddress);
                    if (IsEnabled)
                    {
                        var listenEndpoint = new IPEndPoint(ipAddress ?? IPAddress.Any, p.listenPort);
                        _udpClient         = new UdpClient(listenEndpoint);
                        _udpClient.BeginReceive(RequestCallback, null);
                    }
                    else if(_udpClient != null)
                    {
                        var udpClient = _udpClient;
                        _udpClient    = null;
                        udpClient.Dispose();
                    }
                });
        }

        LogMessage ParseMessageRenderedCompactJsonFormatter(string json)
        {
            var serializerSettings              = new JsonSerializerSettings();
            var substitutions = new (string,string)[]{};
            if (json.Contains("@t"))
            {
                substitutions = new[]
                {
                    (nameof(LogMessage.Timestamp), "@t"),
                    (nameof(LogMessage.Level), "@l"),
                    (nameof(LogMessage.Message), "@m"),
                    (nameof(LogMessage.Id), "@i"),
                    (nameof(LogMessage.MessageTemplate), "@mt"),
                    ("Context", "SourceContext"),
                };
            }

            LogMessage message = null;
            try
            {
                serializerSettings.ContractResolver =
                    new DicitionaryContractResolver(substitutions.ToDictionary(x => x.Item1, x => x.Item2));
                message = JsonConvert.DeserializeObject<LogMessage>(json, serializerSettings);
            }
            catch (Exception e)
            {
                message = new LogMessage()
                {
                    ParseErrors = e.Message
                };
            }

            var jobject = JObject.Parse(json);

            message.RawMessage = jobject.ToString(Formatting.Indented);
            message.ReceiverName = Name;
            var sw = new StringWriter();
            try
            {
                LogEventReader.ReadFromString(json).RenderMessage(sw);
                message.Message = sw.ToString();
            }
            catch
            {
                jobject.TryGetValue("message", StringComparison.InvariantCultureIgnoreCase, out var msg);
                message.Message = msg?.ToString();
            }

            if (message.Id == default) message.Id = (id++).ToString();
            if (message.Level == default(LogEventLevel))
            {
                message.Level = LogEventLevel.Information;
            }
            foreach (var sub in substitutions)
            {
                message.RawMessage = message.RawMessage.Replace(sub.Item2, sub.Item1);
            }
            return message;
        }

        private void RequestCallback(IAsyncResult ar)
        {
            if (_udpClient == null)
                return;
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, IPEndPoint.MaxPort);
            var buffer = _udpClient.EndReceive(ar, ref sender);
            var message = ParseMessageRenderedCompactJsonFormatter(Encoding.UTF8.GetString(buffer));
            message.SenderName = sender.ToString();
            Messages.OnNext(message);
            _udpClient.BeginReceive(RequestCallback, null);
        }

        public void Dispose()
        {
            var udpClient = _udpClient;
            _udpClient    = null;
            udpClient.Dispose();
            Messages.OnCompleted();
            Messages.Dispose();
        }
    }
}
