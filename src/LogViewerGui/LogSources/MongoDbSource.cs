﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LogViewerGui.LogSources;
using LogViewerGui.Model;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace LogViewerGui.MessageSources
{
    public class MongoDbSource:ReactiveObject, ILogSource
    {
        static MongoDbSource()
        {
            BsonClassMap.RegisterClassMap<LogMessage>(cm =>
            {
                cm.MapMember(c => c.Timestamp);
                cm.MapMember(c => c.Level);
                cm.MapMember(c => c.Message).SetElementName("RenderedMessage");
                cm.MapMember(c => c.MessageTemplate).SetElementName("MessageTemplate");
            });
        }
        private IMongoCollection<BsonDocument> _collection;
        
        [Reactive][DataMember]public string Name { get; set; }
        [Reactive][DataMember]public bool IsEnabled { get; set; } = true;
        [Reactive][DataMember]public string DbUrl { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public Subject<LogMessage> Messages { get;private set; } = new Subject<LogMessage>();
        public MongoDbSource()
        {
            this.WhenAnyValue(vm => vm.DbUrl, vm => vm.IsEnabled, (dbUrl, isEnabled) => new {dbUrl, isEnabled})
                .Throttle(TimeSpan.FromMilliseconds(500))
                .Where(p => p.isEnabled && !String.IsNullOrWhiteSpace(p.dbUrl))
                .Subscribe(p =>
                {
                    var url    = new MongoUrl(p.dbUrl);
                    var client = new MongoClient(p.dbUrl);
                    var db      = client.GetDatabase(url.DatabaseName);
                    _collection = db.GetCollection<BsonDocument>("log");

                    Task.Run(ReadLog);
                });
            Name = nameof(MongoDbSource);
            
        }

        DateTime oldest = DateTime.MinValue;
        bool SendLogs(IEnumerable<BsonDocument> docs)
        {
            if (!IsEnabled) return false;
            bool sentSome = false;
            foreach (var doc in docs)
            {
                sentSome        = true;
                var lm          = BsonSerializer.Deserialize<LogMessage>(doc);
                lm.Id           = doc["_id"].ToString();
                lm.Context = doc?.GetValue("Properties", null)?.AsBsonDocument.GetValue("SourceContext", null)?.AsString;
                
                lm.RawMessage   = doc.ToJson(new JsonWriterSettings {Indent = true}).Replace("\\r", "\r").Replace("\\n", "\n");
                lm.ReceiverName = Name;
                lm.Timestamp = lm.Timestamp.ToLocalTime();
                Messages.OnNext(lm);

                if(lm.Timestamp > oldest)
                {
                    oldest = lm.Timestamp;
                }
            }

            return sentSome;
        }

        public Task LoadHistorical(DateTime start, DateTime end)
        {
            var logs = _collection.Find(Builders<BsonDocument>.Filter.Gte(nameof(LogMessage.Timestamp), start)
                            & Builders<BsonDocument>.Filter.Lte(nameof(LogMessage.Timestamp), end))
                .ToEnumerable();
            SendLogs(logs);
            return Task.CompletedTask;
        }

        private Task ReadLog()
        {
            var initial = _collection.Find(new BsonDocument())
                .Sort(Builders<BsonDocument>.Sort.Descending("Timestamp"))
                .Limit(100)
                .ToEnumerable();
            SendLogs(initial.Reverse());


            while (IsEnabled)
            {
                var results = _collection
                    .Find(Builders<BsonDocument>.Filter.Gt(nameof(LogMessage.Timestamp), oldest))
                    .SortBy(l => l["Timestamp"]);
                if (!SendLogs(results.ToEnumerable()))
                {
                    Thread.Sleep(250);
                }
            }

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            IsEnabled = false;
            Messages.OnCompleted();
            Messages.Dispose();
        }
    }
}
