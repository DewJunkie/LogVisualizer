﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.Serialization;
using Humanizer;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Serilog.Events;

namespace LogViewerGui.Model
{
    public class LogMessage : ReactiveObject
    {
        string GetInitials(string context)
        {
            // This could be one chained linq, but it seemed more clear like this.
            string GetWordInitials(string words)
            {
                return string.Join("", words.Humanize().Split(' ').Select(w => w.Substring(0, 1)));
            }
            var parts = context.Split('.');
            return string.Join(".", parts.Select(GetWordInitials));
        }
        public LogMessage()
        {
            this.WhenAnyValue(m => m.Context)
                .Where(c => c?.Contains(".") == true)
                .Select(GetInitials)
                .ToPropertyEx(this, x => x.ContextShort);
        }
        [Reactive][DataMember]
        public DateTime Timestamp { get; set; }

        [Reactive][DataMember]
        public LogEventLevel Level { get; set; }

        [Reactive][DataMember]
        public string Id { get; set; }

        [Reactive][DataMember]
        public string Context { get; set; }

        public extern string ContextShort { [ObservableAsProperty] get; }

        [Reactive][DataMember]
        public string Message { get; set; }

        [Reactive][DataMember]
        public string MessageTemplate { get;set; }

        [Reactive]
        public string RawMessage { get; set; }
        [Reactive]
        public string ReceiverName { get; set; }
        [Reactive]
        public string SenderName { get; set; }
        [Reactive]
        public string ParseErrors { get;set; }
    }
}
