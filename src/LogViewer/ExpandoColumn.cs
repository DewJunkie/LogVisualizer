﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.UI.Xaml.Grid;

namespace LogViewer
{
    class ExpandoColumn:GridColumn
    {
        public ExpandoColumn()
        {
            SetCellType(nameof(ExpandoGridRenderer));
        }
    }
}
