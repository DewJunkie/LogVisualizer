﻿using LogViewer.Properties;
using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Helpers;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace LogViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IViewFor<MainWindowViewModel>
    {
        const string SettingsPath = "LogViewer.settings.json";
        public MainWindow()
        {
            InitializeComponent();

            Load();

            DataGridObjects.CellRenderers.Add(nameof(ExpandoGridRenderer), new ExpandoGridRenderer());

            this.DataContext = ViewModel;

            this.Bind(ViewModel, vm => vm.MongoDbUrl, v => v.TextBoxMongoUrl.Text);

            this.WhenAnyValue(v => v.ViewModel.Databases)
                .Subscribe(dbs => ComboBoxDatabases.ItemsSource = dbs);
            this.Bind(ViewModel, vm => vm.SelectedDatabase, v => v.ComboBoxDatabases.SelectedItem);

            this.WhenAnyValue(v => v.ViewModel.Collections)
                .Subscribe(c => ComboBoxCollections.ItemsSource = c);
            this.Bind(ViewModel, vm => vm.SelectedCollection, v => v.ComboBoxCollections.SelectedItem);
            //this.OneWayBind(ViewModel, vm => vm.Databases, v => v.ComboBoxDatabases.ItemsSource);

            this.WhenAnyValue(v => v.ViewModel.CollectionObjects)
                .Subscribe(c =>
                {
                    var expandos = c?.Select(d => new BsonDocument(UnWrap(d.ToDictionary())))
                        .Select(d => MongoDB.Bson.Serialization.BsonSerializer.Deserialize<ExpandoObject>(d));
                        //c?.Select(d => BsonSerializer.Deserialize<ExpandoObject>(d));
                    //if (c != null)
                    //{
                    //    var expandos2 = expandos?.SelectMany(e =>
                    //    {
                    //        var kvp = (e as IEnumerable<KeyValuePair<string, object>>);
                    //        return kvp.Where(k => k.Value.GetType() == typeof(ExpandoObject)).Select(k => k.Key);
                    //    }).Distinct();
                    //    foreach (var expandoCol in expandos2)
                    //    {
                    //        DataGridObjects.Columns.Add(new ExpandoColumn()
                    //        {
                    //            MappingName = expandoCol,
                    //        });
                    //    }
                    //}

                    DataGridObjects.ItemsSource = expandos;
                    TextBoxObjects.Text = c?.ToJson(new JsonWriterSettings() {Indent = true});
                });
        }

        Dictionary<string, object> UnWrap(Dictionary<string, object> d, string level = "")
        {
            if (!String.IsNullOrWhiteSpace(level))
            {
                level += ".";
            }
            var dOut = new Dictionary<string, object>();
            foreach (var key in d.Keys)
            {
                if (d[key] is Dictionary<string, object> d2)
                {
                    var unwrapped = UnWrap(d2, key);
                    foreach (var k2 in unwrapped.Keys)
                    {
                        dOut[k2] = unwrapped[k2];
                    }
                }
                else
                {
                    dOut[level + key] = d[key];
                }
            }

            return dOut;
        }

        public MainWindowViewModel ViewModel { get; set; }
        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = value as MainWindowViewModel; }

        private void Save()
        {
            File.WriteAllText(SettingsPath, JsonConvert.SerializeObject(ViewModel, Formatting.Indented));
        }

        private void Load()
        {
            try
            {
                ViewModel = JsonConvert.DeserializeObject<MainWindowViewModel>(File.ReadAllText(SettingsPath));
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (ViewModel == null)
                {
                    ViewModel = new MainWindowViewModel();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Save();
        }

        private void DataGridObjects_OnAutoGeneratingColumn(object sender, AutoGeneratingColumnArgs e)
        {
            Debug.WriteLine($"Generating {e.Column.MappingName}");
        }

        private void DataGridObjects_OnSelectionChanged(object sender, GridSelectionChangedEventArgs e)
        {
            var expando = DataGridObjects.SelectedItem as dynamic;
            if (expando != null && ((IEnumerable<KeyValuePair<string, Object>>) expando).Any(k => k.Key == "_id"))
            {
                TextBoxObjects.Text = ViewModel.CollectionObjects.Where(o => o["_id"] == expando._id)
                    .ToJson(new JsonWriterSettings() {Indent = true});
            }
            //DataGridObjects.InvalidateRowHeight(DataGridObjects.SelectedIndex);
            //DataGridObjects.GetVisualContainer().InvalidateMeasureInfo();
        }

        GridRowSizingOptions gridRowResizingOptions = new GridRowSizingOptions();
        private void DataGridObjects_OnQueryRowHeight(object sender, QueryRowHeightEventArgs e)
        {
            //if (DataGridObjects.GridColumnSizer.GetAutoRowHeight(e.RowIndex, gridRowResizingOptions, out var Height))
            //{
            //    e.Height = Height;
            //    e.Handled = true;
            //}
        }
    }
}
