﻿using Humanizer;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace LogViewer
{
    public class MainWindowViewModel:ReactiveObject
    {
        private MongoClient _client;
        public MainWindowViewModel()
        {
            this.WhenAnyValue(vm => vm.MongoDbUrl)
                .Throttle(1.Seconds())
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(_UriChanged);

            this.WhenAnyValue(vm => vm.SelectedDatabase)
                .Subscribe(db => _DbChanged(db));

            this.WhenAnyValue(vm => vm.SelectedCollection)
                .Subscribe(c => _SelectedCollectionChanged(c));

        }

        private async Task _SelectedCollectionChanged(string selectedCollection)
        {
            var db = _client.GetDatabase(SelectedDatabase);
            CollectionObjects = 
                db.GetCollection<BsonDocument>(selectedCollection)
                .AsQueryable()
                .Take(50);
        }

        private async Task _DbChanged(string dbName)
        {
            var db = _client.GetDatabase(dbName);
            var collections = await db.ListCollections()
                .ToListAsync();
            Collections = new ReactiveList<string>(collections
                .Select(t => (string) t["name"]));
        }

        private void _UriChanged(Uri uri)
        {
            try
            {
                _client = new MongoClient(new MongoUrl(uri.ToString()));
                Databases = new ReactiveList<string>(
                    _client.ListDatabases()
                        .ToList()
                        .Select(c => new { Name = (string)c["name"], Size = (double)c["sizeOnDisk"] })
                        .Select(c => c.Name)
                );
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //throw;
            }
        }

        [DataMember][Reactive] public Uri MongoDbUrl { get; set; }
        [Reactive] public ReactiveList<string> Databases { get; set; }
        [DataMember][Reactive] public string SelectedDatabase { get; set; }
        [Reactive] public ReactiveList<string> Collections { get; set; }   
        [DataMember][Reactive] public string SelectedCollection { get; set; }
        [Reactive] public IEnumerable<BsonDocument> CollectionObjects { get; set; }
    }
}
