﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Cells;
using Syncfusion.Windows.PropertyGrid;

namespace LogViewer
{
    class ExpandoGridRenderer:GridVirtualizingCellRendererBase<SfDataGrid, SfDataGrid>
    {
        public override void OnInitializeDisplayElement(DataColumnBase dataColumn, SfDataGrid uiElement, object dataContext)
        {
            uiElement.SetBinding(SfDataGrid.ItemsSourceProperty, new Binding()
            {
                Path = new PropertyPath(dataColumn.GridColumn.MappingName),
            });
        }

        public override void OnUpdateDisplayBinding(DataColumnBase dataColumn, SfDataGrid uiElement, object dataContext)
        {
            throw new NotImplementedException();
        }

        public override void OnInitializeTemplateElement(DataColumnBase dataColumn, ContentControl uiElement, object dataContext)
        {
            throw new NotImplementedException();
        }

        public override void OnUpdateTemplateBinding(DataColumnBase dataColumn, ContentControl uiElement, object dataContext)
        {
            throw new NotImplementedException();
        }

        public override void OnInitializeEditElement(DataColumnBase dataColumn, SfDataGrid uiElement, object dataContext)
        {
            throw new NotImplementedException();
        }

        public override void OnUpdateEditBinding(DataColumnBase dataColumn, SfDataGrid element, object dataContext)
        {
            throw new NotImplementedException();
        }
    }
}
